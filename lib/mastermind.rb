require 'colorize'

class Code
  attr_reader :pegs
  PEGS = { 'r' => :red, 'g' => :green, 'b' => :blue,
           'y' => :yellow, 'o' => :orange, 'p' => :purple }.freeze

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(input)
    parsed = input.downcase.chars
    parsed.each { |char| return nil unless PEGS.key? char }
    Code.new(parsed)
  end

  def self.random
    Code.new(Array.new(4) { PEGS.keys.sample })
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(other)
    @pegs.each_index.count { |i| self[i] == other[i] }
  end

  def near_matches(other)
    @pegs.uniq.reduce(0) do |acc, peg|
      acc + [@pegs.count(peg), other.pegs.count(peg)].min
    end - exact_matches(other)
  end

  def ==(other)
    other.is_a?(Code) && other.pegs == @pegs
  end

  def display

  end
end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
    @guesses = 10
  end

  def get_guess
    print "You have #{@guesses} guesses left. Enter your guess: "
    guess_code = Code.parse(gets.chomp)
    unless guess_code.nil?
      @guesses -= 1
      display_matches(guess_code)
    end
  end

  def display_matches(code)
    exact = @secret_code.exact_matches(code)
    near = @secret_code.near_matches(code)
    puts "Matches: exact #{exact}, near #{near}"
  end

  def winner?(code)
    @secret_code.exact_matches(code) == 4
  end
end
