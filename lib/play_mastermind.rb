require 'colorize'
require_relative 'mastermind'

game = Game.new
print "The computer has thought of a four-digit code. Colors: "
Code::PEGS.each { |k, v| print k.upcase.colorize(v) }
print "\n"
loop do
  guess_code = game.get_guess
  game.display_matches(guess_code)
  if game.winner?(guess_code)
    print 'You win!'
    break
  elsif game.guesses.zero?
    print 'You lose!'
  end
end
